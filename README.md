# go-docker-compose

Example application demonstrating how to use Docker compose with Go applications. The repository contains a simple application written in Golang that contains a single API to display the "Quote of the day".

The app fetches the quote of the day from a public API hosted at `http://quotes.rest/`, then it caches the result in Redis. For subsequent API calls, the app will return the result from Redis cache instead of fetching it from the public API.

# check results
$ go to http://localhost:8080/qod
If I work as hard as I can, I wonder how much I can do in a day?
